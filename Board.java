package com.assessment;

import java.util.LinkedList;
import java.util.List;

public class Board {

	
	public static List getvalue(Integer boardvalue){
		
		Integer retval = 0;
		String msg = "";
		switch (boardvalue) {
		case 2:
			retval = 38;
			msg = "landed on ladder";
			break;
		case 7:
			retval = 14;
			msg = "landed on ladder";
			break;
		case 8:
			retval = 31;
			msg = "landed on ladder";
			break;
		case 15:
			retval = 26;
			msg = "landed on ladder";
			break;
		case 21:
			retval = 42;
			msg = "landed on ladder";
			break;
		case 28:
			retval = 84;
			msg = "landed on ladder";
			break;
		case 36:
			retval = 44;
			msg = "landed on ladder";
			break;
		case 51:
			retval = 67;
			msg = "landed on ladder";
			break;
		case 78:
			retval = 98;
			msg = "landed on ladder";
			break;
		case 71:
			retval = 91;
			msg = "landed on ladder";
			break;
		case 87:
			retval = 94;
			msg = "landed on ladder";
			break;
		case 16:
			retval = 6;
			msg = "landed on snake";
			break;
		case 49:
			retval = 11;
			msg = "landed on snake";
			break;
		case 62:
			retval = 19;
			msg = "landed on snake";
			break;
		case 64:
			retval = 60;
			msg = "landed on snake";
			break;
		case 74:
			retval = 53;
			msg = "landed on snake";
			break;
		case 89:
			retval = 68;
			msg = "landed on snake";
			break;
		case 92:
			retval = 88;
			msg = "landed on snake";
			break;
		case 95:
			retval = 75;
			msg = "landed on snake";
			break;
		case 99:
			retval = 80;
			msg = "landed on snake";
			break;
		
		default:
			retval = boardvalue;
			msg = "";
			break;
		}
		List list = new LinkedList();
		list.add(retval);
		list.add(msg);
		return list;
	}
	
	public String getgamestatus(Player player1, Player player2){
		String str = "";
		if(player1.getCurrentstatus()==100)
		{
			str =  "Player1 wins\n Game over";
		}
		else {
			if(player2.getCurrentstatus()==100){
				str = str+ "Player2 wins\n Game over";
			}
		else
		{
			str = str+"Current status of players: ";
			if(!(player1.getStatusmsg().isEmpty()))
			{
				str = str+"player1 is "+player1.getStatusmsg();
			}
			str = str+" player1 is at "+player1.getCurrentstatus() + ", ";
			if(!(player2.getStatusmsg().isEmpty()))
			{
				str = str+"player2 is "+player2.getStatusmsg();
			}
			str = str+" player2 is at "+player2.getCurrentstatus()+"!";
		}
	}
		return str;
	}
	
public String getgamestatus(Player player1){
		
		if(player1.getCurrentstatus()>=100)
			return player1+" wins";
		
		return player1+" is at "+player1.getCurrentstatus() +"!";
	}
}
