package com.assessment;

import java.util.Scanner;

public class SnakeAndLadderGame {

	public static void main(String[] args) {
		String givenval = null;
		Integer count = 0;
		System.out.println("\t\t--------------Snake and Ladder game-----------------\n\n\n");
		Player A = new Player(100);
		Player B = new Player(12);
		
		Board bd = new Board();
		Scanner sc = new Scanner(System.in);
		System.out.println("Do you want to continue the Game(y/n):");
		givenval = sc.next();
		while(givenval.equalsIgnoreCase("y"))
		{
			if(A.getCurrentstatus()!=100 && B.getCurrentstatus()!=100)
			{
				if(count<10)
				{
					count++;
					A.dicethrown();
					B.dicethrown();
					System.out.println("Do you want to continue the Game(y/n):");
					givenval = sc.next();
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		System.out.println(bd.getgamestatus(A,B));
	}

}
